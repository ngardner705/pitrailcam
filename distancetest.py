#!/usr/bin/python

from __future__ import division
import os
from io import BytesIO
from picamera import PiCamera
import time
import threading
from PIL import Image
import RPi.GPIO as GPIO
from shutil import copyfile
from shutil import copytree
from shutil import move

print "Starting up..."

#camera setup
camera = PiCamera()
imageStreamBuffer = []
camera.resolution = (640,480)

#config
pulseStart = 0
pulseReplyTime = 0
goodDistanceRead = 0
currentDistance = 0
previousDistance = 0
hasMotion = 0
hasEvent = 0
cooledDown = 1
cooldownTime = 6
distanceReads = [1]
eventId = 0

#ultrasonic sensor setup
pinUltrasonic = 17
GPIO.setmode(GPIO.BCM)
GPIO.setup(pinUltrasonic, GPIO.OUT)
	
#camera takes 2 seconds, ultrasonic takes 1 second
time.sleep(3)
print "READY"

try:
	while True:
		
		# capture an image
		imageStream = BytesIO()
		camera.capture(imageStream,format='jpeg')
		imageStreamBuffer.append(imageStream)
		
		# if buffer too big, remove oldest
		if(len(distanceReads) > 20):
			distanceReads.pop(0)
		if(len(imageStreamBuffer) > 5):
			imageStreamBuffer.pop(0)
		
		# cooldown
		time.sleep(0.02)
		
		# up pulse
		GPIO.setup(pinUltrasonic, GPIO.OUT)
		GPIO.output(pinUltrasonic, 1)
		time.sleep(0.000005)
		GPIO.output(pinUltrasonic, 0)
		pulseStart = time.time()
		goodDistanceRead = 0
		
		# listen for it
		GPIO.setup(pinUltrasonic, GPIO.IN)
		
		while GPIO.input(pinUltrasonic)==0:
			#print "LOW"
			starttime=time.time()
			pulseReplyTime=time.time()-pulseStart
			if(pulseReplyTime > 0.5):
				print "TO LONG, NO REPLY"
				break
		while GPIO.input(pinUltrasonic)==1:
			endtime=time.time()
			goodDistanceRead = 1
			#print "HIGH"
		#print "measured"
		
		if goodDistanceRead:
			duration=endtime-starttime
			distance=int(duration*34000/4)
			#print "distance is " + str(distance)
			
			# see if its within range of the past 5
			currentDistance = int(sum(distanceReads) / len(distanceReads))
			#print "distance is " + str(currentDistance)
			
			distanceReads.append(distance)
			
			print currentDistance
			
			if(currentDistance != previousDistance):
				print "MOTION!!!"
			
			previousDistance = currentDistance
			
		else:
			print "no ping back, trying again"
		
except KeyboardInterrupt:
	print "STOPPED"
	GPIO.cleanup()

print "End"