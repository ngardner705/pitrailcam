#!/usr/bin/python

from __future__ import division
import os
from io import BytesIO
from picamera import PiCamera
import time
import threading
from PIL import Image
import RPi.GPIO as GPIO
from shutil import copyfile
from shutil import copytree
from shutil import move

print "Starting up..."

#camera setup
camera = PiCamera()
imageStreamBuffer = []
camera.resolution = (640,480)

#config
pulseStart = 0
pulseReplyTime = 0
goodDistanceRead = 0
currentDistance = 0
previousDistance = 0
cooldownTime = 5
distanceReads = [1]
eventId = 0
eventLive = 0
eventBaseDir = '/home/pi/Projects/piTrailCam/images/'
eventDir = ''
eventUpdate = 0
calibrateStartTime = 0
calibrateTime = 15

#ultrasonic sensor setup
pinUltrasonic = 17
GPIO.setmode(GPIO.BCM)
GPIO.setup(pinUltrasonic, GPIO.OUT)
	
#camera takes 2 seconds
time.sleep(3)
print "Camera ready"
print "Calibrating ultrasonic sensor..."
calibrateStartTime = time.time()

try:
	while True:
			
		# capture an image
		imageStream = BytesIO()
		camera.capture(imageStream,format='jpeg')
		imageStreamBuffer.append(imageStream)
		
		# if buffer too big, remove oldest
		if(len(distanceReads) > 20):
			distanceReads.pop(0)
		if(len(imageStreamBuffer) > 5):
			imageStreamBuffer.pop(0)
		
		# cooldown
		time.sleep(0.02)
		
		# up pulse
		GPIO.setup(pinUltrasonic, GPIO.OUT)
		GPIO.output(pinUltrasonic, 1)
		time.sleep(0.000005)
		GPIO.output(pinUltrasonic, 0)
		pulseStart = time.time()
		goodDistanceRead = 0
		
		# listen for it
		GPIO.setup(pinUltrasonic, GPIO.IN)
		
		while GPIO.input(pinUltrasonic)==0:
			#print "LOW"
			starttime=time.time()
			pulseReplyTime=time.time()-pulseStart
			if(pulseReplyTime > 0.5):
				print "TO LONG, NO REPLY"
				break
		while GPIO.input(pinUltrasonic)==1:
			endtime=time.time()
			goodDistanceRead = 1
			#print "HIGH"
		#print "measured"
		
		if goodDistanceRead:
			duration=endtime-starttime
			distance=int(duration*34000/4)
			#print "distance is " + str(distance)
			
			# see if its within range of the past 5
			currentDistance = int(sum(distanceReads) / len(distanceReads))
			#print "distance is " + str(currentDistance)
			
			distanceReads.append(distance)
			
			if((time.time() - calibrateStartTime) > calibrateTime):
				
				if(currentDistance != previousDistance):
					print "SEE YOU!"
					if(eventLive == 0):
						print "##NEW EVENT##"
						eventId = str(int(time.time()))
						eventLive = 1
						
						# mark event directory
						eventDir = eventBaseDir + 'event-'+eventId
						os.mkdir(eventDir)
						
						# save buffered images to dir
						b = 0
						for i in imageStreamBuffer:
							i.seek(0)
							preimage = Image.open(i)
							preimage.save(eventDir+"/motion-" + str(b) + ".jpg")
							b += 1
					else:
						camera.capture(eventDir+"/motion-" + str(time.time()) + ".jpg")
					eventUpdate = time.time()
				else:
					if eventLive:
						if((time.time() - eventUpdate) > cooldownTime):
							eventLive = 0
						else:
							print "watching"
							camera.capture(eventDir+"/motion-" + str(time.time()) + ".jpg")
					else:
						print "monitoring"
			else:
				print "calibrating..."
				
			previousDistance = currentDistance
			
		else:
			print "no ping back, trying again"
		
except KeyboardInterrupt:
	print "STOPPED"
	GPIO.cleanup()

print "End"