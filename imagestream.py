#!/usr/bin/python

import os
from io import BytesIO
from picamera import PiCamera
import time
import threading
from PIL import Image
import RPi.GPIO as GPIO
from shutil import copyfile
from shutil import copytree
from shutil import move

print "Starting up..."

#camera setup
camera = PiCamera()
imageStreamBuffer = []
camera.resolution = (640,480)

#config
hasMotion = 0
hasEvent = 0
cooledDown = 1
cooldownTime = 6

#pir setup
pinPIR = 17
GPIO.setmode(GPIO.BCM)
GPIO.setup(pinPIR, GPIO.IN) #Read output from PIR motion sensor

#functions
def coolDownTimer():
	global hasEvent
	global GPIO
	if GPIO.input(pinPIR) == 1:
		print "# still capturing #"
		timer = threading.Timer(1, coolDownTimer)
		timer.start()
		return
	else:
		eventFinished()		

def eventFinished():
	global hasEvent
	print "#### EVENT OVER ####"
	hasEvent = 0
	
#camera takes 2 seconds, pir takes 1 second
time.sleep(3)
print "READY"

try:
	while True:
		
		# capture an image
		imageStream = BytesIO()
		camera.capture(imageStream,format='jpeg')
		imageStreamBuffer.append(imageStream)
		#print "image saved to buffer"
		
		# if buffer too big, remove oldest frame
		if(len(imageStreamBuffer) > 5):
			imageStreamBuffer.pop(0)
			#print "buffer too big, removing a frame"
		
		# see if there is motion
		hasMotion = GPIO.input(pinPIR)
		if hasMotion == 1:
			print "#### EVENT START ####"
			eventDir = '/home/pi/Projects/piTrailCam/images/event-'+str(time.time())
			os.mkdir(eventDir)
			hasEvent = 1
			cooledDown = 0
			b = 0
			for i in imageStreamBuffer:
				i.seek(0)
				preimage = Image.open(i)
				preimage.save(eventDir+"/motion-a-" + str(b) + ".jpg")
				b += 1
			while hasMotion:
				camera.capture(eventDir+"/motion-b-" + str(time.time()) + ".jpg")
				hasMotion = GPIO.input(pinPIR)

		if hasMotion == 0:
			if hasEvent == 1:
				timer = threading.Timer(cooldownTime, coolDownTimer)
				timer.start()
			while hasEvent == 1:
				print "# monitoring event #"
				camera.capture(eventDir+"/motion-c-" + str(time.time()) + ".jpg")
			else:
				print ".. scanning ... : " + str(time.time())

except KeyboardInterrupt:
	print "STOPPED"
	GPIO.cleanup()

print "End"